﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor
{
    public class ExtendedResult
    {
        private int _PagesNo, _ResultsNo, _BytesNo;
        public ExtendedResult(int PagesNo, int ResultsNo, int BytesNo)
        {
            this._PagesNo = PagesNo;
            this._ResultsNo = ResultsNo;
            this._BytesNo = BytesNo;
        }
        public int PagesNo
        {
            get { return _PagesNo; }
            set { _PagesNo = value; }
        }
        public int ResultsNo
        {
            get { return _ResultsNo; }
            set { _ResultsNo = value; }
        }
        public int BytesNo
        {
            get { return _BytesNo; }
            set { _BytesNo = value; }
        }
    }
}
