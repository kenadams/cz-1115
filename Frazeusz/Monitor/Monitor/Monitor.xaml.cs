﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;

namespace Monitor
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class Monitor : UserControl
    {
        BindingList<RealtimeGraphItem> items1 = new BindingList<RealtimeGraphItem>();

        BindingList<RealtimeGraphItem> items2 = new BindingList<RealtimeGraphItem>();

        BindingList<RealtimeGraphItem> items3 = new BindingList<RealtimeGraphItem>();
        Righthand.RealtimeGraph.RealtimeGraphControl[] graphs;

        private DispatcherTimer timer;
        private DateTime start;
        private DateTime last;
        private double[] values;
        private int[] lowCounter;
        double[] down;

       
        public Monitor()
        {
            InitializeComponent();
            // bind items
            lowCounter = new int[3];
            values = new double[3];
            down = new double[3];
            graphs = new Righthand.RealtimeGraph.RealtimeGraphControl[3];
            graphs[0] = Graph1;
            graphs[1] = Graph2;
            graphs[2] = Graph3;

            Graph1.SeriesSource = items1;

            Graph2.SeriesSource = items2;

            Graph3.SeriesSource = items3;

            Graph2.Visibility = Visibility.Hidden;
            Graph3.Visibility = Visibility.Hidden;
            timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(250)
            };
            timer.Tick += timer_Tick;
            last = start = DateTime.Now;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            // add new items each tick
            TimeSpan span = DateTime.Now - last;
            TimeSpan totalSpan = DateTime.Now - start;
            int previousTime1 = items1.Count > 0 ? items1[items1.Count - 1].Time : 0;
            int previousTime2 = items2.Count > 0 ? items2[items2.Count - 1].Time : 0;
            int previousTime3 = items3.Count > 0 ? items3[items3.Count - 1].Time : 0;

            adjustGraphScale();

                
            RealtimeGraphItem newItem1 = new RealtimeGraphItem
            {
                Time = (int)(previousTime1 + span.TotalMilliseconds),
                Value = values[0]
            };
            RealtimeGraphItem newItem2 = new RealtimeGraphItem
            {
                Time = (int)(previousTime2 + span.TotalMilliseconds),
                Value = values[1]
            };
            RealtimeGraphItem newItem3 = new RealtimeGraphItem
            {
                Time = (int)(previousTime3 + span.TotalMilliseconds),
                Value = values[2]
            };

            items1.Add(newItem1);
            items2.Add(newItem2);
            items3.Add(newItem3);
            last = DateTime.Now;
            for (int i = 0; i < 3; i++)
            {
                if (down[i] == 0)
                    down[i] = values[i] / 10;
                if (values[i] > 0)
                    values[i] -= down[i];
                    
            }
        }

        private void adjustGraphScale()
        {
            for (int i = 0; i < 3; i++ )
            {
                if (values[i] < graphs[i].MaxY / 10)
                    lowCounter[i]++;
                else
                    lowCounter[i] = 0;
                if (lowCounter[i] > 370)
                {
                    lowCounter[i] = 0;
                    if (graphs[i].MaxY > 10)
                    {
                        graphs[i].HorizontalLinesInterval /= 10;
                        graphs[i].MaxY = graphs[i].MaxY / 10;
                    }
                }

                if (graphs[i].MaxY < values[i])
                {
                    graphs[i].HorizontalLinesInterval *= 10;
                    graphs[i].MaxY *= 10;
                }
            }

        }

        private void Strony_Click(object sender, RoutedEventArgs e)
        {
            Graph1.Visibility = Visibility.Visible;
            Graph2.Visibility = Visibility.Hidden;
            Graph3.Visibility = Visibility.Hidden;
        }

        private void Bajty_Click(object sender, RoutedEventArgs e)
        {
            Graph1.Visibility = Visibility.Hidden;
            Graph2.Visibility = Visibility.Visible;
            Graph3.Visibility = Visibility.Hidden;
        }

        private void Wyniki_Click(object sender, RoutedEventArgs e)
        {
            Graph1.Visibility = Visibility.Hidden;
            Graph2.Visibility = Visibility.Hidden;
            Graph3.Visibility = Visibility.Visible;
        }

        public void update(ExtendedResult result)
        {
            values[0] = result.PagesNo;
            values[1] = result.BytesNo;
            values[2] = result.ResultsNo;
            down[0] = down[1] = down[2]=0;
        }
        

 
       

    }
}
