﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Righthand.RealtimeGraph;
namespace Monitor
{
    public class RealtimeGraphItem : IGraphItem
    {
        public int Time { get; set; }
        public double Value { get; set; }
    }

}
