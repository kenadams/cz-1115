﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using NMock2;
using NUnit.Framework;

namespace Ploter
{
    [TestFixture]
    class DataAnalyzerTest
    {
        private Mockery _mockery;
        private Collection<ExtendedResult> _results;
        private DataAnalyzer _dataAnalyzer;

        [SetUp]
        public void SetUp()
        {
            _mockery = new Mockery();
            _results = new Collection<ExtendedResult>
            {
                new ExtendedResult("onet.pl", "Ala ma kota", 2),
                new ExtendedResult("interia.pl", "Jan Kowalski", 3),
                new ExtendedResult("wp.pl", "Routery", 5)
            };
            _dataAnalyzer = new DataAnalyzer(_results);
        }

        [Test]
        public void TestProcessGraphData()
        {
            var patternIDs = new List<int> {2, 7, 5};
            var domains = new List<string> { "onet.pl", "interia.pl", "wp.pl" };
            Assert.NotNull(_dataAnalyzer.processGraphData(patternIDs, domains));
            var expected = new ObservableCollection<KeyValuePair<string, int>>
            {
                new KeyValuePair<string, int>("onet.pl", 1),
                new KeyValuePair<string, int>("interia.pl", 0),
                new KeyValuePair<string, int>("wp.pl", 1)
            };
            CollectionAssert.AreEqual(expected, _dataAnalyzer.processGraphData(patternIDs, domains));
        }

        [TearDown]
        public void TearDown()
        {
            _mockery = null;
            _results = null;
            _dataAnalyzer = null;
        }
    }
}
