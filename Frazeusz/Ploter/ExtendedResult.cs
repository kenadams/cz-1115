﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ploter
{
    public class ExtendedResult
    {
        public DateTime IncomeTime { get; private set; }
        public bool IsChecked { get; set; }
        public string Url { get; private set; }
        public int PatternId { get; private set; }
        public string Sentence { get; private set; }
        public string Pattern { get; set; }

        public ExtendedResult(string url, string sentence, int patternId)
        {
            this.IncomeTime = DateTime.Now;
            this.IsChecked = false;
            this.Sentence = sentence;
            this.PatternId = patternId;
            this.Url = url;
        }

    }
}
