﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crawler;
using Frazeusz.PatternMatcher;
//using Crawler;
using Parser;
using System.Collections.ObjectModel;

namespace Ploter 
{


    public class MainPloter : IReturnResult
    {

        //private IPatternGUI _patternMatcherGUI;
        //private ICrawlerGUI _crawlerGUI
        //private IMonitor _monitor;
        //private ICrawler _crawler;
        //private IMonitor _monitor;
        //private Object _putResultLock = new Object();
        private MainParser _parser;
        private Collection<ExtendedResult> _results;
        private Collection<Frazeusz.PatternMatcher.PatternMatcher> _patterns;

        //probably here will go IMonitor, ICrawlerGUI, IPatternGUI and Collection<ExtendedResult>
        public MainPloter(Collection<ExtendedResult> resultsContainer)
        {
            this._results = resultsContainer;
        }

        public void putResult(Result result)
        {
            ExtendedResult extendedResult = new ExtendedResult(result.Url, result.Sentence, result.PatternMatcherID);
            foreach (Frazeusz.PatternMatcher.PatternMatcher pattern in _patterns)
                if (pattern.getId() == result.PatternMatcherID)
                    extendedResult.Pattern = pattern.ToString; 
            lock(_results)
            {
                _results.Add(extendedResult);
            }
        }

        public bool StartSearch()
        {
            //List<IPatternMatcher> patterns = _patternMatcherGUI.Patterns;
            //if (!patterns.Any())
             //   return false;
            //Configuration configuration = _crawlerGUI.Configuration;
            //List<UriDepth> uris = _crawlerGUI.Uris;
            //if (!uris.Any())
            //  return false;
            //_parser = new MainParser(this, patterns);

            //parser? list of urisdepth?
            //_crawler = new Crawler.Crawler(new Configuration());
            //_parser.SetCrawler(_crawler);
            //ICrawlerStatistics stats = _crawler.Statistics;
            //_monitor.SetCrawlerStatistics(stats);
            _parser.Run();
            //_crawler.Start();
            return true;
        }

        public bool StopSearch()
        {
            if (IsSearchInProgress()) {
                //_crawler.Stop();
                return true;
            }
            return false;
        }

        public bool IsSearchInProgress()
        {
            //if (_crawler == null || !_crawler.IsRunning)
            //    return false;
            //else
                return true;
        }

    }
}
