﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Ploter
{
    class DataAnalyzer
    {
        private Collection<ExtendedResult> _results;

        public DataAnalyzer(Collection<ExtendedResult> results)
        {
            this._results = results;
        }

        public ObservableCollection<KeyValuePair<string, int>> processGraphData(List<int> patternIds, List<string> domains)
        {
            ObservableCollection<KeyValuePair<string, int>> processResult;
            Dictionary<string, int> domainToOccurences = new Dictionary<string, int>();
            foreach (string domain in domains)
                domainToOccurences.Add(domain, 0);
            foreach (ExtendedResult result in _results)
                if (domains.Contains(result.Url))
                    if(patternIds.Contains(result.PatternId))
                        domainToOccurences[result.Url] = domainToOccurences[result.Url] + 1;
            processResult = new ObservableCollection<KeyValuePair<string, int>>(domainToOccurences.ToList());

            return processResult;
        }

    }
}
