﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crawler
{
    public interface ICrawlerUrisQueue
    {
        void Enqueue(Uri parentUri, IEnumerable<Uri> uris);
    }
}
