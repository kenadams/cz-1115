﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternMatching;
using AsyncWebClient;
using Ploter;

namespace Parser
{
    class HTMLParserFactory : AbstractParserFactory
    {
        override public AbstractParser[] CreateParsers(List<PatternMatcher> patternMatchers, Crawler crawler, IReturnResult ploter,MainParser mainParser)
        {
            HTMLParser[] HTMLParsers = new HTMLParser[_threadsToCreate];

            for (int i = 0; i < _threadsToCreate; i++)
            {
                HTMLParsers[i] = new HTMLParser(patternMatchers, crawler, ploter,mainParser);
            }

            return HTMLParsers;
        }
    }
}
