﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pdf;
using PatternMatching;
using AsyncWebClient;
using Ploter;
using System.Text.RegularExpressions;
using System.IO;

namespace Parser
{
    class PDFParser : AbstractParser
    {
        public PDFParser(List<PatternMatcher> patternMatchers, Crawler crawler, IReturnResult ploter,MainParser mainParser) : base(patternMatchers,crawler,ploter,mainParser)
        {
        }

        override public List<String> Parse(String content)
        {
            PDFToText pdfParser = new PDFToText();
            String text = pdfParser.ExtractTextFromPDFBytes(GetBytes(content));
            FindUrls(text);
            List<String> sentences = new List<String>();

            Regex rx = new Regex(@"(\S.+?[.!?])(?=\s+|$)");
            foreach (Match match in rx.Matches(text))
                sentences.Add(match.Value);

            return sentences;
        }

        override public List<String> Parse(byte[] content)
        {
            PDFToText pdfParser = new PDFToText();
            String text = pdfParser.ExtractText(new MemoryStream(content));
            FindUrls(text);
            List<String> sentences = new List<String>();

            Regex rx = new Regex(@"(\S.+?[.!?])(?=\s+|$)");
            foreach (Match match in rx.Matches(text))
                sentences.Add(match.Value);

            return sentences;
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        override public Document GetDocument()
        {
            Document document = null;
            while ((document = _mainParser.GetDocument(SearchableContentTypes.PDF)) == null) ;
            return document;
        }


    }
}
