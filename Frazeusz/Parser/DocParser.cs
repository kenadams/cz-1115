﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternMatching;
using AsyncWebClient;
using Ploter;
using System.Text.RegularExpressions;
using Docx;
using System.IO;

namespace Parser
{
    class DocParser : AbstractParser
    {
        public DocParser(List<PatternMatcher> patternMatchers, Crawler crawler, IReturnResult ploter,MainParser mainParser) : base(patternMatchers,crawler,ploter,mainParser)
        {
        }
        
       

        override public List<String> Parse(String content)
        {
            Stream stream = new MemoryStream(GetBytes(content));
            DocxToText docxParser = new DocxToText(stream);
            String text = docxParser.ExtractText();
            FindUrls(text);
            List<String> sentences = new List<String>();

            Regex rx = new Regex(@"(\S.+?[.!?])(?=\s+|$)");
            foreach (Match match in rx.Matches(text))
                sentences.Add(match.Value);

            return sentences;
        }

        override public List<String> Parse(byte[] content)
        {

            Stream stream = new MemoryStream(content);
            DocxToText docxParser = new DocxToText(stream);
            String text = docxParser.ExtractText();
            FindUrls(text);
            List<String> sentences = new List<String>();

            Regex rx = new Regex(@"(\S.+?[.!?])(?=\s+|$)");
            foreach (Match match in rx.Matches(text))
                sentences.Add(match.Value);

            return sentences;
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        override public Document GetDocument()
        {
            Document document = null;
            while ((document = _mainParser.GetDocument(SearchableContentTypes.DOC)) == null) ;
            return document;
        }


    }
}
