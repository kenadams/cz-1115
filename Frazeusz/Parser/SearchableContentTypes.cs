﻿using System;
using System.Linq;
using System.Text;

namespace Parser
{
    public enum SearchableContentTypes
    {
        PDF,
        DOC,
        HTML
    }
}
