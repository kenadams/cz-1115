﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frazeusz.PatternMatcher
{
    interface IPatternMatcher
    {
        string ToString { get; }
        int getId();
        string FindPattern(string sentence);
    }
}
