﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frazeusz.PatternMatcher
{
    public enum PatternType
    {
        Plural,
        Synonyms,
        Inflections,
        Forms,
    }
}
